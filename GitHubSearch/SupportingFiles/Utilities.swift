//
//  Utilities.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation
import UIKit

final class Utilities {
    
    /// Creating UILabel with predefined parameters
    ///
    /// - Parameters:
    ///     - text: String that will be placed as a text
    ///     - font: UIFont for the label
    ///     - textAlignment: NSTextAlignment defining the alignment of the text
    ///     - textColor: UIColor defining the textColor
    ///     - numberOfLines: Int defining the number of lines for the label
    /// - Returns: UILabel with the previosuly sent parameters
    static func createLabelWith(text: String?, font: UIFont, textAlignment: NSTextAlignment, textColor: UIColor, numOfLines: Int) -> UILabel {
        let label = UILabel()
        
        label.text = text
        label.font = font
        label.textAlignment  = textAlignment
        label.textColor = textColor
        label.numberOfLines = numOfLines
        
        return label
    }
    
    /// Creating UIButton with predefined parameters
    ///
    /// - Parameters:
    ///     - title: String that will be placed as a title of the button
    ///     - backgroundColor: UIColor for the button's background
    ///     - cornerRadius: CGFloat defining the corner radius of the button
    ///     - textColor: UIColor defining the textColor
    ///     - font: UIFont for the label
    /// - Returns: UIButton with the previosuly sent parameters
    static func createButtonWith(title: String?, backgroundColor: UIColor, cornerRadius: CGFloat, textColor: UIColor, font: UIFont) -> UIButton {
        let button = UIButton()
        
        button.setTitle(title, for: .normal)
        button.backgroundColor = backgroundColor
        button.setTitleColor(textColor, for: .normal)
        button.layer.cornerRadius = cornerRadius
        button.titleLabel?.font = font
        
        return button
    }
    
    /// Creating UIImageView with predefined parameters
    ///
    /// - Parameters:
    ///     - imageName: String? that represents the name of the local image
    ///     - backgroundColor: UIColor for the imageView's background
    ///     - cornerRadius: CGFloat defining the corner radius of the imageView
    ///     - contentMode: UIImageView.ContentMode for the imageView
    /// - Returns: UIImageView with the previosuly sent parameters
    static func createImageViewWith(imageName: String?, backgroundColor: UIColor, cornerRadius: CGFloat, contentMode: UIImageView.ContentMode) -> UIImageView {
        let imageView = UIImageView()
        
        if let localImageName = imageName {
            imageView.image = UIImage(named: localImageName)
        }
        
        imageView.backgroundColor = backgroundColor
        imageView.layer.cornerRadius = cornerRadius
        imageView.contentMode = contentMode
        
        return imageView
    }
    
    /// Formatting an integer into a number with friendlyK. For example the number 1500 will be represented as 1.5K
    ///
    /// - Parameters:
    ///     - number: Int that represents the number to be formatted
    /// - Returns: String that will represent a number formated into a string with friendly K.
    static func formatIntoFriendlyK(for number: Int) -> String {
        let num = abs(Double(number))
        let sign = (number < 0) ? "-" : ""
        
        switch num {
        case 1_000_000_000...:
            var formatted = num / 1_000_000_000
            formatted = formatted.reduceScale(to: 1)
            return "\(sign)\(formatted)B"
            
        case 1_000_000...:
            var formatted = num / 1_000_000
            formatted = formatted.reduceScale(to: 1)
            return "\(sign)\(formatted)M"
            
        case 1_000...:
            var formatted = num / 1_000
            formatted = formatted.reduceScale(to: 1)
            return "\(sign)\(formatted)K"
            
        case 0...:
            return "\(number)"
            
        default:
            return "\(sign)\(number)"
        }
    }
    
    /// Formatting a date represented in String that we want to
    ///
    /// - Parameters:
    ///     - number: Int that represents the number to be formatted
    ///     - dateString: String? that represents the date that we want to format
    ///     - fromDateFormat: DateFormat which represent the current dateFormat of the date we want to format
    ///     - toDateFormat: DateFormat which represent the new dateFormat of the date we want to format into string
    /// - Returns: String that will represent the date sent as a parameter with the new date format
    static func getFormatedDateString(from dateString: String?, fromDateFormat: String, toDateFormat: String) -> String {
        let date = DateFormatterHelper.getDateFromString(string: dateString, dateFormat: fromDateFormat)
        
        return DateFormatterHelper.getDateStringWithFormat(dateFormat: toDateFormat, date: date)
    }
    
    /// Getting the current app environment type
    /// - Returns: Enum type from AppEnvironment
    static func getAppEnvironmentType() -> AppEnvironmentType {
        if Bundle.main.bundleIdentifier == "com.nenadljubik.GitHubSearch-test" {
            return .test
        } else if Bundle.main.bundleIdentifier == "com.nenadljubik.GitHubSearch-staging" {
            return .staging
        } else {
            return .production
        }
    }
}
