//
//  Extensions.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation
import UIKit

extension Double {
    /// Rounds the double to decimal places value
    /// - Parameter places: Int indicating the number of places
    /// - Returns: Double that will reduce the number
    func reduceScale(to places: Int) -> Double {
        let multiplier = pow(10, Double(places))
        let newDecimal = multiplier * self // move the decimal right
        let truncated = Double(Int(newDecimal)) // drop the fraction
        let originalDecimal = truncated / multiplier // move the decimal back
        return originalDecimal
    }
}

extension UIViewController {
    /// Helper function to present an alert with custom title and Ok action
    /// - Parameter title: String that represent the title of the alert
    func presentAlert(with title: String) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    /// Helper function to open an URL in the browser
    /// - Parameter linkString: String representation of the URL that should be open in the browser
    func openURLInBrowser(linkString: String?) {
        if let link = linkString, let url = URL(string: link) {
            UIApplication.shared.open(url)
        }
    }
}
