//
//  Enumerations.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation

enum SortType: String {
    case stars = "Stars"
    case forks = "Forks"
    case updated = "Updated"
}

enum NetworkRequestState {
    case success
    case failure(error: Error)
}

enum RepositoryTableViewType {
    case repositorySearch
    case repositoryDetails
}

enum DateFormat: String {
    case apiDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    case longDateFormat = "dd.MM.yyyy"
}

enum AppEnvironmentType {
    case test
    case staging
    case production
}

enum APIError: Error {
    case customErrorWithMessage(message: String) // if server response cannot be correctly parsed
    case generalMessage
    
    public func title() -> String {
        switch self {
        case .customErrorWithMessage(let message):
            return message
        case .generalMessage:
            return "Something went wrong. Try again"
        }
    }
}
