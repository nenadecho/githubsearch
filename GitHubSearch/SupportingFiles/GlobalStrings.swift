//
//  GlobalStrings.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation

struct GlobalStrings {

    static let watchers = "Watchers:"
    static let forks = "Forks:"
    static let issues = "Issues:"
    static let notAvailable = "N\\A"
    static let authorName = "Author Name:"
    static let authorType = "Author Type:"

    struct RepositoryListScreen {
        static let sortBy = "Sort By"
    }

    struct RepositoryDetailsScreen {
        static let repositoryName = "Repository Name:"
        static let details = "Details"
        static let repositoryDetails = "Repository Details:"
        static let visitGitHub = "Visit Repository On GitHub"
        static let dateOfCreation = "Date Of Creation:"
        static let dateOfModification = "Date Of Last Modification:"
        static let programmingLanguage = "Programming Language:"
        static let repositoryInfo = "Repository Info"
        static let author = "Author"
    }

    struct UserDetailsScreen {
        static let visitAuthorProfile = "Visit Author's Profile"
    }
}
