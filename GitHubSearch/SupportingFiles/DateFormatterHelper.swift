//
//  DateFormatterHelper.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 13.7.21.
//

import Foundation

final class DateFormatterHelper {

    private static let dateFormatter = DateFormatter()

    /// Converting string to date
    ///
    /// - Parameters:
    ///     - string: String? representation of the date
    ///     - dateFormat: String representation of the desired format of the date
    /// - Returns: Converted optional date fromt string to date.
    static func getDateFromString(string: String?, dateFormat: String) -> Date? {
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: string ?? "")
    }

    /// Converting date to specified date format
    ///
    /// - Parameters:
    ///     - dateFormat: String representation of the desired format of the date
    ///     - date: Date? that should be formated
    /// - Returns: String that represents the formated date in the desired format or empty string if the date is nil
    static func getDateStringWithFormat(dateFormat: String, date: Date?) -> String {
        dateFormatter.dateFormat = dateFormat

        if let unwrappedDate = date {
            return dateFormatter.string(from: unwrappedDate)
        } else {
            return ""
        }
    }
}
