//
//  AuthorTableViewCell.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 13.7.21.
//

import UIKit

final class AuthorTableViewCell: UITableViewCell {

    // Properties
    private var authorNameLabel: UILabel!
    private var authorProfileImageView: UIImageView!
    private var authorTypeLabel: UILabel!

    // Constants
    private struct Constants {
        static var authorProfileImageResizeFactor =  3.5
    }

    // MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        authorNameLabel.text = ""
        authorTypeLabel.text = ""
        authorProfileImageView.image = nil
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        authorProfileImageView.layer.cornerRadius = contentView.frame.size.width/CGFloat(Constants.authorProfileImageResizeFactor)/2
    }

    // MARK: - Setting Up Views And Constraints
    private func setupViews() {
        selectionStyle = .none

        authorProfileImageView = Utilities.createImageViewWith(imageName: nil, backgroundColor: .systemGroupedBackground, cornerRadius: 0, contentMode: .scaleAspectFill)
        authorProfileImageView.kf.indicatorType = .activity
        authorProfileImageView.layer.masksToBounds = true
        authorProfileImageView.isUserInteractionEnabled = true
        authorProfileImageView.hero.id = "imageTransition"

        authorNameLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .left, textColor: .black, numOfLines: 2)

        authorTypeLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .left, textColor: .black, numOfLines: 2)

        contentView.addSubview(authorProfileImageView)
        contentView.addSubview(authorNameLabel)
        contentView.addSubview(authorTypeLabel)
    }

    private func setupConstraints() {
        authorProfileImageView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.width.equalTo(contentView).dividedBy(Constants.authorProfileImageResizeFactor)
            make.height.equalTo(authorProfileImageView.snp.width)
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-20).priority(.low)
        }

        authorNameLabel.snp.makeConstraints { make in
            make.bottom.equalTo(authorProfileImageView.snp.centerY).offset(-5)
            make.left.equalTo(authorProfileImageView.snp.right).offset(20)
            make.right.equalToSuperview().offset(-20)
        }

        authorTypeLabel.snp.makeConstraints { make in
            make.top.equalTo(authorProfileImageView.snp.centerY).offset(5)
            make.left.equalTo(authorProfileImageView.snp.right).offset(20)
            make.right.equalToSuperview().offset(-20)
        }
    }

    // MARK:- Setting Up Cell With Author
    func setupCell(with repositoryAuthor: Author?) {
        guard let author = repositoryAuthor else { return }

        if let avatarString = author.avatarURL, let avatarURL = URL(string: avatarString) {
            authorProfileImageView.kf.setImage(with: avatarURL, options: [.transition(.fade(0.5))])
        }

        authorNameLabel.text = "\(GlobalStrings.authorName) \(author.login ?? GlobalStrings.notAvailable)"
        authorTypeLabel.text = "\(GlobalStrings.authorType) \(author.type ?? GlobalStrings.notAvailable)"
    }
}
