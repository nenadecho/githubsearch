//
//  RepositoryDetailsTableViewCell.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 13.7.21.
//

import UIKit
import RxSwift

final class RepositoryDetailsTableViewCell: UITableViewCell {

    // Properties
    private var repositoryNameLabel: UILabel!
    private var repositoryDescriptionLabel: UILabel!
    private var numOfWatchersLabel: UILabel!
    private var numOfForksLabel: UILabel!
    private var numOfIssuesLabel: UILabel!
    private var horizontalStackView: UIStackView!
    private var programmingLanguageLabel: UILabel!
    private var dateCreationLabel: UILabel!
    private var dateModificationLabel: UILabel!
    private var verticalStackView: UIStackView!
    private(set) var repositoryURLButton: UIButton!
    private(set) var disposeBag = DisposeBag()

    // MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    // MARK: - Setting Up Views And Constraints
    private func setupViews() {
        selectionStyle = .none

        repositoryNameLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .left, textColor: .black, numOfLines: 0)

        repositoryDescriptionLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .left, textColor: .black, numOfLines: 0)

        numOfWatchersLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 14, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 1)

        numOfForksLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 14, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 1)

        numOfIssuesLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 14, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 1)

        programmingLanguageLabel = Utilities.createLabelWith(text: GlobalStrings.RepositoryDetailsScreen.dateOfCreation, font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 0)

        dateCreationLabel = Utilities.createLabelWith(text:GlobalStrings.RepositoryDetailsScreen.dateOfCreation, font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 0)

        dateModificationLabel = Utilities.createLabelWith(text: GlobalStrings.RepositoryDetailsScreen.dateOfModification, font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines:0)

        setupHorizontalStackView()
        setupVerticalStackView()

        repositoryURLButton = Utilities.createButtonWith(title: GlobalStrings.RepositoryDetailsScreen.visitGitHub, backgroundColor: .systemBlue, cornerRadius: 8, textColor: .white, font: .systemFont(ofSize: 16, weight: .semibold))

        contentView.addSubview(repositoryNameLabel)
        contentView.addSubview(repositoryDescriptionLabel)
        contentView.addSubview(horizontalStackView)
        contentView.addSubview(verticalStackView)
        contentView.addSubview(repositoryURLButton)
    }

    private func setupConstraints() {
        repositoryNameLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
        }

        repositoryDescriptionLabel.snp.makeConstraints { make in
            make.left.right.equalTo(repositoryNameLabel)
            make.top.equalTo(repositoryNameLabel.snp.bottom).offset(20)
        }

        horizontalStackView.snp.makeConstraints { make in
            make.left.right.equalTo(repositoryNameLabel)
            make.top.equalTo(repositoryDescriptionLabel.snp.bottom).offset(20)
        }

        verticalStackView.snp.makeConstraints { make in
            make.left.right.equalTo(repositoryNameLabel)
            make.top.equalTo(horizontalStackView.snp.bottom).offset(20)
        }

        repositoryURLButton.snp.makeConstraints { make in
            make.left.right.equalTo(repositoryNameLabel)
            make.height.equalTo(45)
            make.top.equalTo(verticalStackView.snp.bottom).offset(20)
            make.bottom.equalToSuperview().offset(-20)
        }
    }

    // MARK: - Setting Up Stack Views
    private func setupHorizontalStackView() {
        horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .equalSpacing
        horizontalStackView.spacing = 20

        horizontalStackView.addArrangedSubview(numOfWatchersLabel)
        horizontalStackView.addArrangedSubview(numOfForksLabel)
        horizontalStackView.addArrangedSubview(numOfIssuesLabel)
    }

    private func setupVerticalStackView() {
        verticalStackView = UIStackView()
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .equalSpacing
        verticalStackView.spacing = 20

        verticalStackView.addArrangedSubview(programmingLanguageLabel)
        verticalStackView.addArrangedSubview(dateCreationLabel)
        verticalStackView.addArrangedSubview(dateModificationLabel)
    }

    // MARK:- Setting Up Cell With Repository
    func setupCell(with repository: Repository) {
        repositoryNameLabel.text = "\(GlobalStrings.RepositoryDetailsScreen.repositoryDetails) \(repository.fullName ?? GlobalStrings.notAvailable)"

        repositoryDescriptionLabel.text = "\(GlobalStrings.RepositoryDetailsScreen.repositoryDetails) \(repository.description ?? GlobalStrings.notAvailable)"

        numOfWatchersLabel.text = "\(GlobalStrings.watchers) \(Utilities.formatIntoFriendlyK(for: repository.watchers ?? 0))"
        numOfForksLabel.text = "\(GlobalStrings.forks) \(Utilities.formatIntoFriendlyK(for: repository.forks ?? 0))"
        numOfIssuesLabel.text = "\(GlobalStrings.issues) \(Utilities.formatIntoFriendlyK(for: repository.openIssues ?? 0))"

        programmingLanguageLabel.text = "\(GlobalStrings.RepositoryDetailsScreen.programmingLanguage) \(repository.programmingLanguage ?? "")"

        dateCreationLabel.text = "\(GlobalStrings.RepositoryDetailsScreen.dateOfCreation) \(Utilities.getFormatedDateString(from: repository.dateOfCreation, fromDateFormat: DateFormat.apiDateFormat.rawValue, toDateFormat: DateFormat.longDateFormat.rawValue))"

        dateModificationLabel.text = "\(GlobalStrings.RepositoryDetailsScreen.dateOfModification) \(Utilities.getFormatedDateString(from: repository.dateOfLastModification, fromDateFormat: DateFormat.apiDateFormat.rawValue, toDateFormat: DateFormat.longDateFormat.rawValue))"
    }
}
