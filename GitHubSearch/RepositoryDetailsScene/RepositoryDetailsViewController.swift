//
//  RepositoryDetailsViewController.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 13.7.21.
//

import UIKit

final class RepositoryDetailsViewController: UIViewController {

    // Properties
    fileprivate var mainView: RepositorySearchView { return view as! RepositorySearchView }
    private var viewModel: RepositoryDetailsViewModel!
    private var repositoryTableViewType: RepositoryTableViewType!
    private var repository: Repository!

    // MARK: - Controller Initialization
    init(repository: Repository, repoTableViewType: RepositoryTableViewType) {
        self.repositoryTableViewType = repoTableViewType
        self.repository = repository
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK:- Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view = RepositorySearchView(repositoryTableViewType: repositoryTableViewType)
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.title = GlobalStrings.RepositoryDetailsScreen.details

        mainView.tableView.delegate = self
        mainView.tableView.dataSource = self
    }
    
    // MARK: - Subscribing To RepositoryURLButton Tapps
    private func subscribeToRepositoryURLButton(cell: RepositoryDetailsTableViewCell) {
        cell.repositoryURLButton.rx.tap
            .bind { [weak self] in
                self?.openURLInBrowser(linkString: self?.repository.gitHubLink)
            }.disposed(by: cell.disposeBag)
    }
}

// MARK: - UITableView Delegate And DataSource Methods
extension RepositoryDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? GlobalStrings.RepositoryDetailsScreen.repositoryInfo : GlobalStrings.RepositoryDetailsScreen.author
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "repoDetailsCell", for: indexPath) as! RepositoryDetailsTableViewCell
            cell.setupCell(with: repository)
            subscribeToRepositoryURLButton(cell: cell)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "authorCell", for: indexPath) as! AuthorTableViewCell
            cell.setupCell(with: repository.repoAuthor)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Only when the AppEnvironment is production and the selected row is in the section for the author the user is navigated to the UserDetailsScreen
        if indexPath.section == 1 && Utilities.getAppEnvironmentType() == .production {
            navigationController?.pushViewController(UserDetailsViewController(author: repository.repoAuthor, isPresentedWithAnimation: false), animated: true)
        }
    }
}
