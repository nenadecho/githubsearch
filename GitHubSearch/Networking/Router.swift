//
//  Router.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {

    case SearchRepoWith(query: String, perPage: Int, page: Int, sortType: SortType)
    var method: Alamofire.HTTPMethod {
        switch self {
        case .SearchRepoWith:
            return .get
        }
    }

    var path: String {
        switch self {
        case .SearchRepoWith(let query, let perPage, let page, let sortType):
            return (NetworkConstants.Network.baseUrl + NetworkConstants.Network.Endpoints.searchRepos + "?q=\(query)" + "&per_page=\(perPage)" + "&page=\(page)" + "&sort=\(sortType.rawValue.lowercased())" + "&order=desc").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? NetworkConstants.Network.baseUrl + NetworkConstants.Network.Endpoints.searchRepos
        }
    }

    var parameters: [String : Any] {
        switch self {
        default:
            return [:]
        }
    }

    var header : [String: String] {
        return [
            "Content-Type" : "application/json"
        ]
    }

    func asURLRequest() throws -> URLRequest {
        var url: URL!
        url = URL(string: path)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        if method.rawValue != "GET" {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: self.parameters, options: .prettyPrinted)
        }

        urlRequest.allHTTPHeaderFields = header
        return urlRequest
    }
}
