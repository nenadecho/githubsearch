//
//  RepositorySearchService.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa

final class RepositorySearchService {

    static func searchReposWith(query: String, perPage: Int, page: Int, sortType: SortType) -> Observable<Result<RepositoryResponse, Error>> {
        return Observable<Result<RepositoryResponse, Error>>.create { observer in
            AF.request(Router.SearchRepoWith(query: query, perPage: perPage, page: page, sortType: sortType)).responseDecodable(of: RepositoryResponse.self) { response in
                switch response.result {
                case .success(let reposResponse):
                    if let errorMessage = reposResponse.message {
                        observer.onNext(Result.failure(APIError.customErrorWithMessage(message: errorMessage)))
                    }

                    if let _ = reposResponse.repositories {
                        observer.onNext(Result.success(reposResponse))
                    } else {
                        observer.onNext(Result.failure(APIError.customErrorWithMessage(message: "No repositories found.")))
                    }

                case .failure(let error):
                    observer.onNext(Result.failure(error))
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}
