//
//  NetworkConstants.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation

struct NetworkConstants {
    struct Network {
        static let baseUrl = "https://api.github.com/"

        struct Endpoints {
            static let searchRepos = "search/repositories"
        }
    }
}
