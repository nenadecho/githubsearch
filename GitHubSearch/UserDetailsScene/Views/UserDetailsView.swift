//
//  UserDetailsView.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 13.7.21.
//

import UIKit

final class UserDetailsView: UIView {

    // Properties
    private var authorNameLabel: UILabel!
    private var authorProfileImageView: UIImageView!
    private var authorTypeLabel: UILabel!
    private(set) var dismissButton: UIButton!
    private(set) var visitAuthorProfileButton: UIButton!

    // Constants
    private struct Constants {
        static var authorProfileImageResizeFactor =  1.8
        static var visitAuthorProfileButtonResizeFactor =  1.2
    }

    // MARK: - View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK:- Setting Up Views And Constraints
    private func setupViews() {
        backgroundColor = .systemGroupedBackground

        authorProfileImageView = Utilities.createImageViewWith(imageName: nil, backgroundColor: .gray, cornerRadius: UIScreen.main.bounds.width/CGFloat(Constants.authorProfileImageResizeFactor)/2, contentMode: .scaleAspectFill)
        authorProfileImageView.kf.indicatorType = .activity
        authorProfileImageView.layer.masksToBounds = true
        authorProfileImageView.hero.id = "imageTransition"
        authorProfileImageView.hero.modifiers = [.cascade, .scale(0.5)]

        dismissButton = Utilities.createButtonWith(title: nil, backgroundColor: UIColor.black.withAlphaComponent(0.5), cornerRadius: 35/2, textColor: .clear, font: .systemFont(ofSize: .zero))
        dismissButton.setImage(UIImage(named: "dismiss"), for: .normal)

        authorNameLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 2)

        authorTypeLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 2)
        
        visitAuthorProfileButton = Utilities.createButtonWith(title: GlobalStrings.UserDetailsScreen.visitAuthorProfile, backgroundColor: .systemBlue, cornerRadius: 8, textColor: .white, font: .systemFont(ofSize: 16, weight: .semibold))

        addSubview(authorProfileImageView)
        addSubview(dismissButton)
        addSubview(authorNameLabel)
        addSubview(authorTypeLabel)
        addSubview(visitAuthorProfileButton)
    }

    private func setupConstraints() {
        authorProfileImageView.snp.makeConstraints { make in
            make.width.equalToSuperview().dividedBy(Constants.authorProfileImageResizeFactor)
            make.height.equalTo(authorProfileImageView.snp.width)
            make.top.equalToSuperview().offset(100)
            make.centerX.equalToSuperview()
        }

        dismissButton.snp.makeConstraints { make in
            make.height.width.equalTo(34)
            make.top.equalTo(safeAreaLayoutGuide).offset(20)
            make.right.equalToSuperview().offset(-20)
        }

        authorNameLabel.snp.makeConstraints { make in
            make.top.equalTo(authorProfileImageView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(20)
            make.right.equalTo(snp.centerX).offset(-20)
        }

        authorTypeLabel.snp.makeConstraints { make in
            make.top.equalTo(authorNameLabel)
            make.left.equalTo(snp.centerX).offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        visitAuthorProfileButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-40)
            make.height.equalTo(45)
            make.width.equalToSuperview().dividedBy(Constants.visitAuthorProfileButtonResizeFactor)
        }
    }

    func setupView(with author: Author?) {
        if let imageString = author?.avatarURL, let avatarURL = URL(string: imageString) {
            authorProfileImageView.kf.setImage(with: avatarURL, options: [.transition(.fade(0.5))])
        }

        authorNameLabel.text = "\(GlobalStrings.authorName) \(author?.login ?? GlobalStrings.notAvailable)"
        authorTypeLabel.text = "\(GlobalStrings.authorType) \(author?.type ?? GlobalStrings.notAvailable)"
    }
    
    func setDimissButtonVisibility(shouldShow: Bool) {
        dismissButton.isHidden = !shouldShow
    }
}
