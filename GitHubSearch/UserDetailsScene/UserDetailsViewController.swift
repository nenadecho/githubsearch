//
//  UserDetailsViewController.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 13.7.21.
//

import UIKit
import RxSwift

final class UserDetailsViewController: UIViewController {

    // Properties
    fileprivate var mainView: UserDetailsView { return view as! UserDetailsView }
    private var viewModel: UserDetailsViewModel!

    private var disposeBag = DisposeBag()
    private var author: Author?
    private var isPresentedWithAnimation: Bool!

    // MARK: - Controller Initialization
    init(author: Author?, isPresentedWithAnimation: Bool) {
        super.init(nibName: nil, bundle: nil)
        self.author = author
        self.isPresentedWithAnimation = isPresentedWithAnimation
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK:- Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view = UserDetailsView()

        mainView.setupView(with: author)
        mainView.setDimissButtonVisibility(shouldShow: isPresentedWithAnimation)

        setupRx()
    }

    // MARK:- Setting Up Rx
    private func setupRx() {
        mainView.dismissButton.rx.tap
            .bind { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }.disposed(by: disposeBag)
        
        mainView.visitAuthorProfileButton.rx.tap
            .bind { [weak self] in
                self?.openURLInBrowser(linkString: self?.author?.profileLink)
            }.disposed(by: disposeBag)
    }
}
