//
//  Author.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation

class Author: Codable {
    var login: String?
    var avatarURL: String?
    var type: String?
    var profileLink: String?

    enum CodingKeys: String, CodingKey {
        case avatarURL = "avatar_url"
        case login
        case type
        case profileLink = "html_url"
    }
}
