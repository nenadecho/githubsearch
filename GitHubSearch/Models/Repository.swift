//
//  Repository.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation

class Repository: Codable {
    var id: Int?
    var name: String?
    var fullName: String?
    var repoAuthor: Author?
    var forks: Int?
    var openIssues: Int?
    var watchers: Int?
    var programmingLanguage: String?
    var dateOfCreation: String?
    var dateOfLastModification: String?
    var description: String?
    var gitHubLink: String?

    enum CodingKeys: String, CodingKey {
        case repoAuthor = "owner"
        case fullName = "full_name"
        case openIssues = "open_issues"
        case forks
        case watchers
        case programmingLanguage = "language"
        case dateOfCreation = "created_at"
        case dateOfLastModification = "updated_at"
        case description
        case gitHubLink = "html_url"
    }
}

class RepositoryResponse: Codable {
    let count: Int?
    let repositories: [Repository]?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case count = "total_count"
        case repositories = "items"
    }

}
