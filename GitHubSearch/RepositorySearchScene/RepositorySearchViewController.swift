//
//  RepositorySearchViewController.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import Hero

final class RepositorySearchViewController: UIViewController {

    fileprivate var mainView: RepositorySearchView { return view as! RepositorySearchView }
    private var viewModel = RepositorySearchViewModel()
    private var disposeBag = DisposeBag()

    // MARK:- Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view = RepositorySearchView(repositoryTableViewType: .repositorySearch)

        setupNavigation()

        mainView.tableView.delegate = self
        mainView.tableView.dataSource = self

        setupRx()
    }

    // MARK:- Setting Up Navigation
    private func setupNavigation() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.title = "Repository Search"

        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Repositories"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self

        searchController.hidesNavigationBarDuringPresentation = false
    }

    private func setupRx() {
        viewModel.repoRequestStatusObservable
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] state in
                switch state {
                case .success:
                    self?.mainView.tableView.reloadData()
                case .failure(let error):
                    self?.presentAlert(with: error.localizedDescription)
                }
            }).disposed(by: disposeBag)

        viewModel.footerIndicatorAnimatingObservable
            .subscribe(onNext: { [weak self] shouldAnimate in
                shouldAnimate ? self?.mainView.footerIndicatorView.startAnimating() : self?.mainView.footerIndicatorView.stopAnimating()
            }).disposed(by: disposeBag)

        viewModel.mainIndicatorAnimatingObservable
            .subscribe(onNext: { [weak self] shouldAnimate in
                shouldAnimate ? self?.mainView.mainIndicatorView.startAnimating() : self?.mainView.mainIndicatorView.stopAnimating()
            }).disposed(by: disposeBag)

        viewModel.internetConnectionObservable
            .filter { hasInternet in
                return hasInternet == false
            }
            .subscribe { [weak self] _ in
                self?.presentAlert(with: "No internet connection")
            }.disposed(by: disposeBag)
    }

    private func createSortMenu(for button: UIButton) {
        let items = UIMenu(title: "", options: .displayInline, children: [
            UIAction(title: SortType.stars.rawValue, state: getUIMenuElementState(for: .stars), handler: { [weak self] _ in
                self?.viewModel.handleSelectionOfSortMenu(with: .stars)
            }),
            UIAction(title: SortType.forks.rawValue, state: getUIMenuElementState(for: .forks), handler: { [weak self] _ in
                self?.viewModel.handleSelectionOfSortMenu(with: .forks)
            }),
            UIAction(title: SortType.updated.rawValue, state: getUIMenuElementState(for: .updated), handler: { [weak self] _ in
                self?.viewModel.handleSelectionOfSortMenu(with: .updated)
            }),
        ])

        button.menu = UIMenu(title: "", children: [items])
        button.showsMenuAsPrimaryAction = true
    }

    private func getUIMenuElementState(for sortType: SortType) -> UIMenuElement.State {
        // Checking which type from the UIMenu is selected so we can know to show it to the user
        return viewModel.checkIfSortTypeIsSelected(sortType: sortType) ? .on : .off
    }
}

// MARK: - UISearchBar Delegate Methods
extension RepositorySearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            viewModel.resetSearchedData()
            viewModel.searchRepos(with: text)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.resetSearchedData()
        mainView.tableView.reloadData()
    }
}

// MARK: - UITableView Delegate And DataSource Methods
extension RepositorySearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repoSearchCell", for: indexPath) as! RepositoryTableViewCell
        cell.setupCell(with: viewModel.repositories[indexPath.row])
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sortView = SortView()
        sortView.setSelectedSortType(sortType: viewModel.sortType)
        createSortMenu(for: sortView.sortTypeButton)
        return sortView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.repositories.count != 0 ? 50 : 0
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.shouldIncrementPageNumber(index: indexPath.row)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utilities.getAppEnvironmentType() != .test {
            navigationController?.pushViewController(RepositoryDetailsViewController(repository: viewModel.repositories[indexPath.row], repoTableViewType: .repositoryDetails), animated: true)
        }
    }
}

// MARK: - RepositoryTableViewCell Delegate Method(s)
extension RepositorySearchViewController: RepositoryTableViewCellDelegate {
    func authorAvatarTapped(author: Author?) {
        if Utilities.getAppEnvironmentType() == .production {
            let userVC = UserDetailsViewController(author: author, isPresentedWithAnimation: true)
            userVC.modalPresentationStyle = .overFullScreen
            userVC.hero.isEnabled = true
            present(userVC, animated: true)
        }
    }
}
