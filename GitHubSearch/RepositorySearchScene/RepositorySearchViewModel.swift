//
//  RepositorySearchViewModel.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation
import RxSwift
import RxCocoa

final class RepositorySearchViewModel {
    
    private struct Constants {
        static var perPage = 20
    }

    private var disposeBag = DisposeBag()

    private(set) var repositories = [Repository]()

    private(set) var sortType = SortType.stars
    private var queryToSearch = ""
    private(set) var page = 1
    private(set) var totalCount = 0
    private(set) var totalPageCount = 0

    private var repoRequestStatusRelay = PublishRelay<NetworkRequestState>()

    lazy var repoRequestStatusObservable: Observable<NetworkRequestState> = {
        return repoRequestStatusRelay.asObservable()
    }()

    private var footerIndicatorRelay = PublishRelay<Bool>()

    lazy var footerIndicatorAnimatingObservable: Observable<Bool> = {
        return footerIndicatorRelay.asObservable()
    }()

    private var mainIndicatorRelay = PublishRelay<Bool>()

    lazy var mainIndicatorAnimatingObservable: Observable<Bool> = {
        return mainIndicatorRelay.asObservable()
    }()

    private var internetConnectionRelay = PublishRelay<Bool>()

    lazy var internetConnectionObservable: Observable<Bool> = {
        return internetConnectionRelay.asObservable()
    }()
    
    init() {
        
    }
    
    // MARK:- Setting Selected Sort Type
    func setSelectedSortType(sortType: SortType) {
        self.sortType = sortType
    }

    // MARK:- Checking If SortType Is Selected
    func checkIfSortTypeIsSelected(sortType: SortType) -> Bool {
        return self.sortType == sortType
    }

    // MARK:- Handling Selection Of SortTypes From UIMenu
    func handleSelectionOfSortMenu(with sortType: SortType) {
        if !checkIfSortTypeIsSelected(sortType: sortType) {
            self.sortType = sortType
            if queryToSearch != "" {
                repositories.removeAll()
                page = 1
                searchRepos(with: queryToSearch)
            }
        }
    }

    // MARK: - Checking If The Search Repository Request Should Be Done With Pagination
    func shouldIncrementPageNumber(index: Int) {
        let currentOffset = page * Constants.perPage
        if (index == currentOffset - Constants.perPage / 2) && page <= totalPageCount {
            footerIndicatorRelay.accept(true)
            page += 1
            searchRepos(with: queryToSearch)
        }
    }

    // MARK: - Resetting Searched Repository Data
    func resetSearchedData() {
        repositories.removeAll()
        page = 1
        queryToSearch = ""
    }

    // MARK:- Showing/Hiding MainIndicator
    private func shouldShowMainIndicator() {
        page == 1 ? (mainIndicatorRelay.accept(true)) : (mainIndicatorRelay.accept(false))
    }

    // MARK:- Calculating the total number of pages for pagination
    private func calculateTotalNumberOfPages(totalResults: Int) {
        let totalPages:Double = Double(totalResults / Constants.perPage)
        totalPageCount = Int(totalPages.rounded(.up))
    }

    // MARK:- Search Repositories
    func searchRepos(with query: String) {
        if !Reachability.isInternetAvailable() {
            internetConnectionRelay.accept(false)
            return
        }
        queryToSearch = query
        shouldShowMainIndicator()
        RepositorySearchService.searchReposWith(query: query, perPage: Constants.perPage, page: page, sortType: sortType).subscribe(onNext: { [weak self] result in
            switch result {
            case .success(let reposResponse):
                self?.totalCount = reposResponse.count ?? 0
                self?.calculateTotalNumberOfPages(totalResults: self?.totalCount ?? 0)
                self?.repositories.append(contentsOf: reposResponse.repositories ?? [])
                self?.repoRequestStatusRelay.accept(.success)
            case .failure(let error):
                self?.repoRequestStatusRelay.accept(.failure(error: error))
            }
            self?.footerIndicatorRelay.accept(false)
            self?.mainIndicatorRelay.accept(false)
        }).disposed(by: disposeBag)
    }
}
