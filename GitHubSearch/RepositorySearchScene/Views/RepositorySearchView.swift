//
//  RepositorySearchView.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import UIKit

final class RepositorySearchView: UIView {

    // Properties
    private(set) var tableView: UITableView!
    private(set) var mainIndicatorView: UIActivityIndicatorView!
    private(set) var footerIndicatorView: IndicatorView!
    private(set) var repositoryTableViewType: RepositoryTableViewType!

    // MARK:- View Initialization
    init(repositoryTableViewType: RepositoryTableViewType) {
        super.init(frame: .zero)
        self.repositoryTableViewType = repositoryTableViewType

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK:- Setting Up Views And Constraints
    private func setupViews() {

        footerIndicatorView = IndicatorView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 50))

        tableView = UITableView(frame: .zero, style: getTableViewStyle())
        tableView.register(RepositoryTableViewCell.self, forCellReuseIdentifier: "repoSearchCell")
        tableView.register(RepositoryDetailsTableViewCell.self, forCellReuseIdentifier: "repoDetailsCell")
        tableView.register(AuthorTableViewCell.self, forCellReuseIdentifier: "authorCell")
        tableView.tableFooterView = footerIndicatorView
        tableView.rowHeight = UITableView.automaticDimension

        mainIndicatorView = UIActivityIndicatorView()
        mainIndicatorView.style = .medium
        mainIndicatorView.hidesWhenStopped = true

        addSubview(tableView)
        addSubview(mainIndicatorView)
    }

    private func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        mainIndicatorView.snp.makeConstraints { make in
            make.center.equalTo(tableView)
        }
    }

    private func getTableViewStyle() -> UITableView.Style {
        return repositoryTableViewType == .repositorySearch ? .plain : .insetGrouped
    }
}
