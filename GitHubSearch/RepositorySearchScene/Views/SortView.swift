//
//  SortRepositoriesView.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import UIKit

final class SortView: UIView {

    // Properties
    private(set) var sortLabel: UILabel!
    private(set) var sortTypeButton: UIButton!
    private(set) var chevronImageView: UIImageView!

    // MARK:- View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK:- Setting Up Views And Constraints
    private func setupViews() {
        backgroundColor = .white
//        isUserInteractionEnabled = true

        sortLabel = Utilities.createLabelWith(text: GlobalStrings.RepositoryListScreen.sortBy, font: .systemFont(ofSize: 14, weight: .regular), textAlignment: .center, textColor: .gray, numOfLines: 1)

        sortTypeButton = Utilities.createButtonWith(title: "", backgroundColor: .clear, cornerRadius: 0, textColor: .black, font: .systemFont(ofSize: 14, weight: .semibold))
        sortTypeButton.showsMenuAsPrimaryAction = true

        chevronImageView = Utilities.createImageViewWith(imageName: "chevronDown", backgroundColor: .clear, cornerRadius: 0, contentMode: .scaleAspectFill)
        chevronImageView.layer.masksToBounds = true

        addSubview(sortLabel)
        addSubview(sortTypeButton)
        addSubview(chevronImageView)
    }

    private func setupConstraints() {
        chevronImageView.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(10)
        }

        sortTypeButton.snp.makeConstraints { make in
            make.right.equalTo(chevronImageView.snp.left).offset(-2)
            make.centerY.equalToSuperview()
            make.height.equalTo(sortLabel)
        }

        sortLabel.snp.makeConstraints { make in
            make.right.equalTo(sortTypeButton.snp.left).offset(-5)
            make.centerY.equalToSuperview()
        }
    }

    func setSelectedSortType(sortType: SortType) {
        sortTypeButton.setTitle(sortType.rawValue, for: .normal)
    }
}

