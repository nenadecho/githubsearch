//
//  RepositoryTableViewCell.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import UIKit
import Kingfisher

protocol RepositoryTableViewCellDelegate {
    func authorAvatarTapped(author: Author?)
}

final class RepositoryTableViewCell: UITableViewCell {

    // Properties
    private var repositoryNameLabel: UILabel!
    private var authorNameLabel: UILabel!
    private var authorProfileImageView: UIImageView!
    private var numOfWatchersLabel: UILabel!
    private var numOfForksLabel: UILabel!
    private var numOfIssuesLabel: UILabel!
    private var stackView: UIStackView!
    private var repository: Repository!

    var delegate: RepositoryTableViewCellDelegate?

    // Constants
    private struct Constants {
        static var authorProfileImageResizeFactor =  7
    }

    // MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: - Setting Up Views And Constraints
    private func setupViews() {
        selectionStyle = .none

        repositoryNameLabel = Utilities.createLabelWith(text: "Repository Name", font: .systemFont(ofSize: 16, weight: .bold), textAlignment: .left, textColor: .black, numOfLines: 1)

        numOfWatchersLabel = Utilities.createLabelWith(text: GlobalStrings.watchers, font: .systemFont(ofSize: 14, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 1)

        numOfForksLabel = Utilities.createLabelWith(text: GlobalStrings.forks, font: .systemFont(ofSize: 14, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 1)

        numOfIssuesLabel = Utilities.createLabelWith(text: GlobalStrings.issues, font: .systemFont(ofSize: 14, weight: .semibold), textAlignment: .center, textColor: .black, numOfLines: 1)

        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 20

        stackView.addArrangedSubview(numOfWatchersLabel)
        stackView.addArrangedSubview(numOfForksLabel)
        stackView.addArrangedSubview(numOfIssuesLabel)

        authorProfileImageView = Utilities.createImageViewWith(imageName: nil, backgroundColor: .systemGroupedBackground, cornerRadius: UIScreen.main.bounds.width/CGFloat(Constants.authorProfileImageResizeFactor)/2, contentMode: .scaleAspectFill)
        authorProfileImageView.kf.indicatorType = .activity
        authorProfileImageView.layer.masksToBounds = true
        authorProfileImageView.isUserInteractionEnabled = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped))
        authorProfileImageView.addGestureRecognizer(tapGesture)

        authorNameLabel = Utilities.createLabelWith(text: "", font: .systemFont(ofSize: 16, weight: .semibold), textAlignment: .left, textColor: .black, numOfLines: 0)

        contentView.addSubview(repositoryNameLabel)
        contentView.addSubview(stackView)
        contentView.addSubview(authorProfileImageView)
        contentView.addSubview(authorNameLabel)
    }

    private func setupConstraints() {
        repositoryNameLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
        }

        stackView.snp.makeConstraints { make in
            make.left.right.equalTo(repositoryNameLabel)
            make.top.equalTo(repositoryNameLabel.snp.bottom).offset(20)
        }

        authorProfileImageView.snp.makeConstraints { make in
            make.left.equalTo(repositoryNameLabel)
            make.width.equalToSuperview().dividedBy(Constants.authorProfileImageResizeFactor)
            make.height.equalTo(authorProfileImageView.snp.width)
            make.top.equalTo(stackView.snp.bottom).offset(20)
            make.bottom.equalToSuperview().offset(-20)
        }

        authorNameLabel.snp.makeConstraints { make in
            make.centerY.equalTo(authorProfileImageView)
            make.left.equalTo(authorProfileImageView.snp.right).offset(20)
            make.right.equalToSuperview().offset(-20)
        }
    }

    func setupCell(with repository: Repository) {
        self.repository = repository
        repositoryNameLabel.text = repository.fullName

        if let avatarString = repository.repoAuthor?.avatarURL, let avatarURL = URL(string: avatarString) {
            authorProfileImageView.kf.setImage(with: avatarURL, options: [.transition(.fade(0.5))])
        }

        numOfWatchersLabel.text? = "\(GlobalStrings.watchers) \(Utilities.formatIntoFriendlyK(for: repository.watchers ?? 0))"
        numOfForksLabel.text? = "\(GlobalStrings.forks) \(Utilities.formatIntoFriendlyK(for: repository.forks ?? 0))"
        numOfIssuesLabel.text? = "\(GlobalStrings.issues) \(Utilities.formatIntoFriendlyK(for: repository.openIssues ?? 0))"

        authorNameLabel.text = repository.repoAuthor?.login
    }

    @objc func imageViewTapped() {
        delegate?.authorAvatarTapped(author: repository.repoAuthor)
    }
}
