//
//  IndicatorView.swift
//  GitHubSearch
//
//  Created by Ненад Љубиќ on 12.7.21.
//

import Foundation
import UIKit

final class IndicatorView: UIView {

    // Properties
    private var indicatorView: UIActivityIndicatorView!

    // MARK:- View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK:- Setting Up Views And Constraints
    private func setupViews() {
        backgroundColor = .clear

        indicatorView = UIActivityIndicatorView()
        indicatorView.style = .medium
        indicatorView.hidesWhenStopped = true

        addSubview(indicatorView)
    }

    private func setupConstraints() {
        indicatorView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    // MARK:- Starting/Stopping IndicatorView
    func startAnimating() {
        indicatorView.startAnimating()
    }

    func stopAnimating() {
        indicatorView.stopAnimating()
    }
}

